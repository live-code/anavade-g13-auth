import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AuthService } from '../../core/auth/auth.service';
import { catchError, map, tap } from 'rxjs/operators';
import { interval, of } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'ava-admin',
  template: `
    <mat-spinner *ngIf="authService.pending"></mat-spinner>

    <li *ngFor="let member of members">
      {{member.name}}
    </li>
    
    <div class="alert alert-danger" *ngIf="error">c'è un errore</div>
    MEMBERS: {{members?.length}}
    
    <button (click)="init()">reload</button>
  `,
})
export class AdminComponent {
  members: any[];
  error: boolean;

  constructor(
    private snackBar: MatSnackBar,
    private http: HttpClient,
    public authService: AuthService
  ) {
    this.init();
  }

  init(): void {
    this.http.get<any[]>('http://localhost:3000/members')
      // ....
      .subscribe(
        res => {
          this.snackBar.open('members loaded', 'GET', {
            duration: 3000
          })
          this.members = res;
        },
        err => {
          this.error = true;
        }
      );
  }

}
