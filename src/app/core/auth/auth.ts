export interface Auth {
  role: string;
  displayName: string;
  token: string;
}
