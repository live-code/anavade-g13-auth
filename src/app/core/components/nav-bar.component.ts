import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'ava-nav-bar',
  template: `
    <nav class="navbar navbar-expand navbar-light bg-light">
      <div class="container-fluid">
        <a class="navbar-brand">Avanade</a>
       
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item" routerLink="login">
              <a class="nav-link" aria-current="page" routerLinkActive="active">login</a>
            </li>
            <li class="nav-item" routerLink="catalog">
                <a class="nav-link" aria-current="page" routerLinkActive="active">catalog</a>
            </li>
            <li class="nav-item" routerLink="admin" *ngIf="authService.ifLogged">
              <a class="nav-link" aria-current="page" routerLinkActive="active">admin</a>
            </li>   
            
            <li class="nav-item" *ngIf="authService.ifLogged">
              <a class="nav-link" aria-current="page" (click)="authService.logout()">
                Quit
              </a>
            </li>
            
            <li class="nav-item" *ngIf="authService.auth">
                <span *ngIf="authService.pending">LOADING....</span>
                <a class="nav-link">{{authService.displayName}}</a>
            </li>
            
          </ul>
        </div>
      </div>
    </nav>
  `,
  styles: [
  ]
})
export class NavBarComponent implements OnInit {

  constructor(public authService: AuthService) { }


  ngOnInit(): void {
  }

}
