import { HttpEvent, HttpHandler, HttpInterceptor, HttpParams, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { AuthService } from './auth.service';
import { catchError, delay, finalize, map, tap } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { environment } from '../../../environments/environment';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(
    private snackBar: MatSnackBar,
    private authService: AuthService
  ) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let cloned = req;
    this.authService.pending = true;

    const params = new HttpParams()
      .set('version', '3');

    if (this.authService.ifLogged && req.url.includes('localhost') ) {
      cloned = req.clone({
        params,
        headers: req.headers.set('Authentication', this.authService.token)
      });
    }
    return next.handle(cloned)
      .pipe(
        delay(environment.production ? 0 : 1000),
        catchError(err => {
          switch (err.status) {
            case 401:
              this.authService.logout();
              break;
            default:
            case 404:
              this.snackBar.open('errore', 'ERROR', {
                duration: 3000
              });
              this.authService.logout();
              break;
          }
          return throwError(null);
        }),
        finalize(() => {
          this.authService.pending = false;
        })
      );
  }

}

// http client
// -----o--o---xo-----------oooo----->

// form
// ---ooooo-----o-----o----o-------->

// router
// ----o-----o-----o--o-o--o------->
