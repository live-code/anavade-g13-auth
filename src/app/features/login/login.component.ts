import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../core/auth/auth.service';

@Component({
  selector: 'ava-login',
  template: `
    
    <div class="alert alert-danger" *ngIf="authService.error">
      ERROR LOGIN
    </div>

    <mat-tab-group>
      <mat-tab label="SigIn">
        <ava-sign-in></ava-sign-in>
      </mat-tab>
      
      <mat-tab label="Lost Password"> 
        .... lost
      </mat-tab>
      
      <mat-tab label="REgistration"> 
        ... registration
      </mat-tab>
    </mat-tab-group>
    
    
  `,
})
export class LoginComponent {
  constructor(public authService: AuthService) { }
}
