import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Auth } from './auth';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root'})
export class AuthService {
  auth: Auth;
  error: boolean;
  pending: boolean;

  constructor(
    private http: HttpClient,
    private router: Router
  ) {
    this.auth = JSON.parse(localStorage.getItem('auth'));
  }

  login(username: string, password: string): void {
    this.error = false;
    const params = new HttpParams()
      .set('username', username)
      .set('params', password);

    this.http.get<Auth>(`http://localhost:3000/login`, { params })
      .subscribe(
        res => {
          this.auth = res;
          localStorage.setItem('auth', JSON.stringify(res))
          this.router.navigateByUrl('admin');
        },
        err => this.error = true
      );
  }

  logout(): void {
    this.auth = null;
    localStorage.removeItem('auth');
    this.router.navigateByUrl('login');
  }

  get ifLogged(): boolean {
    return !!this.auth;
  }

  get displayName(): string {
    return this.auth.displayName;
  }

  get role(): string {
    return this.auth?.role;
  }

  get token(): string {
    return this.auth?.token;
  }
}
