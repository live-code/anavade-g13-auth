import { Component } from '@angular/core';

@Component({
  selector: 'ava-root',
  template: `
    <ava-nav-bar></ava-nav-bar>
    
    <div class="container mt-3">
      <router-outlet></router-outlet>
    </div>
  `,
})
export class AppComponent {

}
