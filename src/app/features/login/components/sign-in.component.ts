import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../core/auth/auth.service';

@Component({
  selector: 'ava-sign-in',
  template: `
    <mat-spinner *ngIf="authService.pending"></mat-spinner>

    
    <form #f="ngForm" (submit)="signHandler(f.value)">

      <mat-form-field>
        <mat-label>Username</mat-label>
        <input matInput type="text" ngModel name="username" required>
      </mat-form-field>

      <br>
      <mat-form-field>
        <mat-label>Password</mat-label>
        <input matInput type="text" ngModel name="password" required>
      </mat-form-field>
  
      <br>
      <button mat-raised-button color="primary" type="submit" [disabled]="f.invalid">LOGIN</button>
    </form>
  `,
  styles: [
  ]
})
export class SignInComponent {

  constructor(public authService: AuthService) { }

  signHandler(formData): void {
    this.authService.login(formData.username, formData.password)
  }


}
